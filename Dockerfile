FROM python:3.11-slim

RUN useradd -m -d /app app

WORKDIR /app

USER 1000
ENV PATH=/app/.local/bin:${PATH}

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY --link jptv.py .

CMD python jptv.py