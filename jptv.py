import atexit
import time
from base64 import b64encode
from concurrent.futures import ThreadPoolExecutor
from functools import wraps
from queue import SimpleQueue, Empty
from signal import signal, SIGTERM, SIGINT, SIGHUP, SIGABRT
from threading import Event
from xml.etree.ElementTree import Element

import defusedxml.ElementTree as ET
import httpx
from deluge_client import DelugeRPCClient
from loguru import logger

import config

http_client = httpx.Client(timeout=7.0)

urls_seen = set()

urls_queue = SimpleQueue()
thread_pool = ThreadPoolExecutor()
is_exit = Event()
first_run = True


def _sleep(duration: int) -> None:
    if duration <= 1:
        time.sleep(duration)
        return None

    for _ in range(duration):
        if is_exit.is_set():
            return None

        time.sleep(1)


def event_loop(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        while not is_exit.is_set():
            func(*args, **kwargs)
    return wrapper


def filter_rss_item(item: Element, feed_config: dict[str]) -> bool:
    # True = keep, False = drop
    for pattern in feed_config.get('blacklist', {}).get('title'):
        if pattern.match(title := item.find('title').text):
            logger.info(f'Dropping torrent based on title {title}')
            return False

    return True


@event_loop
def get_update(feed: dict):
    global first_run

    try:
        resp = http_client.get(feed['url'])
    except Exception as e:
        logger.debug(f'Just an exception "{e}". Retry later Zzzz')
        _sleep(feed['sleep'])
        return None

    root = ET.fromstring(resp.text)

    for item in root.findall('channel/item/enclosure[@url]/..'):
        url = item.find('enclosure').get('url')
        if url in urls_seen:
            continue

        logger.debug(f'Found new {url=}, putting into queue')
        
        urls_seen.add(url)

        if not filter_rss_item(item, feed):
            continue

        if first_run:
            logger.debug('But the torrent is already slate, who knows what is the last time this bot executed.')
        else:
            urls_queue.put(url)
            logger.debug('It is now in the queue')

    first_run = False
    _sleep(feed['sleep'])


@logger.catch()
def download_torrent(url) -> None:
    logger.debug(f'Downloading {url=}')
    torrent_resp = http_client.get(url, timeout=30)
    torrent_resp.raise_for_status()

    torrent = b64encode(torrent_resp.content)

    with DelugeRPCClient(**config.DELUGE) as deluge:
        logger.debug('Connected to deluge')
        hash = deluge.call('core.add_torrent_file', url.split('/')[-1], torrent, {})
        logger.info(f'{hash=}')
        deluge.call('label.set_torrent', hash, 'private')
        deluge.call('core.resume_torrent', hash)

    logger.success('Added one torrent to deluge')


@event_loop
def consumer():
    try:
        url = urls_queue.get(timeout=5)
    except Empty:
        return None

    download_torrent(url)


def main():
    logger.info('started')
    for _ in range(2):
        thread_pool.submit(consumer)

    for feed_config in config.JPTV['rss_feeds']:
        thread_pool.submit(get_update, feed_config)

    while not is_exit.is_set():
        time.sleep(1)


def handle_exit(*args):
    logger.info('exiting')
    is_exit.set()


@atexit.register
def cleanup():
    thread_pool.shutdown()


if __name__ == '__main__':
    for exit_signal in [SIGTERM, SIGINT, SIGHUP, SIGABRT]:
        signal(exit_signal, handle_exit)

    main()
